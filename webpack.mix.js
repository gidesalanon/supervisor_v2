const mix = require('laravel-mix');
require('laravel-mix-svg-vue');

const tailwindcss = require('tailwindcss');
const path = require('path')
/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
 */

mix.js('resources/js/app.js', 'public/js')
  .sass('resources/sass/app.scss', 'public/css')
  .webpackConfig({
    resolve: {
      alias: {
        '@': path.resolve(__dirname, 'resources/js')
      }
    }
  })
  .svgVue()
  .options({
    processCssUrls: false,
    postCss: [tailwindcss('./tailwind.config.js')],
});
