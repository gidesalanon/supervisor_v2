import axiosRestClient from 'axios-rest-client'

const api = axiosRestClient({
  baseUrl: '/api'
})

api.setAccessToken = (accessToken) => {
  api.setHeader('Authorization', `Bearer ${accessToken}`)
  api.setHeader('Content-Type', 'application/json')
}

export default api
