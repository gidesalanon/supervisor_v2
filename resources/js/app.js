require('./bootstrap')
import { router, initalize } from './bootstrap'
import Vue from 'vue'
import App from './App.vue'
import store from '@/store'
import SvgVue from 'svg-vue'

Vue.use(SvgVue)

Vue.component('load-component', require('./components/ExampleComponent.vue'))

Vue.config.productionTip = false

initalize().then(() => {
  new Vue({
    router,
    store,
    render: h => h(App)
  }).$mount('#app');
})
