import Vue from 'vue'
import store from '@/store'

Vue.prototype.$notify = options => {
  options = typeof options === 'string' ? { message: options } : options

  const {
    title = 'Notification',
    message,
    type = 'primary',
    timeout = 3000,
    action = () => {} } = options

  store.dispatch('notify', {
    title,
    message,
    timeout,
    status: type,
    action: action
  })
}
