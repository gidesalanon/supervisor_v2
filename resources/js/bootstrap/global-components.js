import Vue from 'vue'
import upperFirst from 'lodash/upperFirst'
import camelCase from 'lodash/camelCase'

const requireComponent = require.context('../components', false, /(The|Base)[\w-]+\.vue$/)

// For each matching file name...
requireComponent.keys().forEach(fileName => {
  const componentConfig = requireComponent(fileName)
  // Get the PascalCase version of the component name
  const componentName = upperFirst(camelCase(fileName.replace(/\.\w+$/, '')))

  // Globally register the component
  Vue.component(componentName, componentConfig.default || componentConfig)
})
