import Vue from "vue";
import VueRouter from "vue-router";

import Layout from "@/layouts/index.vue";
import Dashboard from "@/pages/dashboard";
import planning from "@/pages/planning";
import Login from "@/pages/login";

import CreateEc from "@/pages/planning/createEc";
import UpdateEc from "@/pages/planning/updateEc";

import DeleteUE from "@/pages/ue/delete";
import DeleteEC from "@/pages/ec/delete";

import CreateEducationalGroup from "@/pages/educationalgroup/create";
import DeleteEducationalGroup from "@/pages/educationalgroup/delete";
import CreateUser from "@/pages/user/create";
import UpdateUser from "@/pages/user/update";

import CreateAcademicYear from "@/pages/academicyear/create";
import UpdateAcademicYear from "@/pages/academicyear/update";
import DeleteAcademicYear from "@/pages/academicyear/delete";

import History from "@/pages/history";

import Settings from "@/pages/settings";

import CreateEnseignant from "@/pages/enseignant/create";

import store from "@/store";

Vue.use(VueRouter);

const router = new VueRouter({
  mode: "history",
  routes: [
    {
      path: "/login",
      name: "login",
      component: Login,
      meta: {
        auth: "guest"
      }
    },
    {
      path: "/",
      component: Layout,
      children: [
        {
          path: "",
          name: "dashboard.index",
          component: Dashboard,
          meta: {
            redirect: "planning.index"
          },
          children: [
            {
              path: "planning/:id",
              name: "planning.index",
              component: planning,
              children: [
                {
                  path: "ec/:ue",
                  name: "ec.create",
                  meta: {
                    auth: ["admin", "resp.gped", "resp.class"]
                  },
                  component: CreateEc
                },
                {
                  path: "ec/:ue/:ec",
                  name: "ec.update",
                  meta: {
                    auth: ["admin", "resp.gped", "resp.class"]
                  },
                  component: UpdateEc
                }
              ]
            }
          ]
        },
        {
          path: "settings",
          name: "settings.index",
          meta: {
            auth: ["admin", "resp.gped"]
          },
          component: Settings
        },

        {
          path: "academicyear/create",
          name: "academicyear.create",
          meta: {
            auth: ["admin"]
          },
          component: CreateAcademicYear
        },
        {
          path: "academicyear/update",
          name: "academicyear.update",
          meta: {
            auth: ["admin", "resp.gped"]
          },
          component: UpdateAcademicYear
        },
        {
          path: "academicyear/delete",
          name: "academicyear.delete",
          meta: {
            auth: ["admin"]
          },
          component: DeleteAcademicYear
        },

        {
          path: "educationalgroup/create",
          name: "educationalgroup.create",
          meta: {
            auth: ["admin", "resp.gped"]
          },
          component: CreateEducationalGroup
        },
        {
          path: "educationalgroup/delete",
          name: "educationalgroup.delete",
          meta: {
            auth: ["admin", "resp.gped"]
          },
          component: DeleteEducationalGroup
        },

        {
          path: "ue/delete",
          name: "ue.delete",
          meta: {
            auth: ["admin", "resp.gped"]
          },
          component: DeleteUE
        },

        {
          path: "ec/delete",
          name: "ec.delete",
          meta: {
            auth: ["admin", "resp.gped"]
          },
          component: DeleteEC
        },

        {
          path: "user/create",
          name: "user.create",
          meta: {
            auth: ["admin", "resp.gped"]
          },
          component: CreateUser
        },
        {
          path: "user/update",
          name: "user.update",
          meta: {
            auth: ["admin", "resp.gped", "resp.class"]
          },
          component: UpdateUser
        },

        {
          path: "history",
          name: "history.index",
          component: History
        },
        {
          path: "enseignant/create",
          name: "enseignant.create",
          meta: {
            auth: ["admin", "resp.gped"]
          },
          component: CreateEnseignant
        },
      ]
    }
  ]
});

router.beforeEach((to, from, next) => {
  const authenticated = store.getters["auth/authenticated"];
  let userRole;
  if (authenticated) {
    userRole = store.getters["auth/user"].role;
  }
  const routeWithAuth = to.matched.find(
    r => r.meta && typeof r.meta.auth !== "undefined"
  );

  if (to.path === "/logout") {
    store.dispatch("auth/logout");
    return next("/login");
  }

  if (routeWithAuth) {
    if (
      (!routeWithAuth.meta.auth.includes(userRole) ||
        routeWithAuth.meta.auth === "guest") &&
      authenticated
    ) {
      return next("/");
    }
  } else if (!authenticated) {
    return next({ name: "login", query: { redirect: to.fullPath } });
  }

  next();
});

export default router;
