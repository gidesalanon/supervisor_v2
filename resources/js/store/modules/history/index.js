import getters from './getters'
import actions from './actions'

export default {
  getters,
  actions,
  namespaced: true
}
