export default {
  history (_, { query }) {
    return query().withAll().get()
  }
}
