import api from '@/api'

export default {

  fetch: ({ dispatch }) => {
    return api.history().then((response) => {
      if (response.isSuccessful) {
        const data = response.data
        dispatch('insertOrUpdate', { data })
      }
      return response
    }).catch((error) => {
      console.error(error)
      throw error
    })
  }
}
