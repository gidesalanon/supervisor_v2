export default {
  /**
   * Notifications
   */
  notify ({ commit }, notif) {
    commit('ADD_NOTIFICATION', notif)
  }
}
