import { updateField } from 'vuex-map-fields'

export default {
  updateField,

  ADD_NOTIFICATION (state, notif) {
    notif.key = state.notifications.length
    state.notifications.push(notif)
  }
}
