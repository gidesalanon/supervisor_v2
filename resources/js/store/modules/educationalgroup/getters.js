import _countBy from "lodash/countBy";

const getters = {
  educationalgroup(_, { query }) {
    return query()
      .withAll()
      .get();
  },

  educationalgroupByYear(_, { query }) {
    return id =>
      query()
        .where("annee_academiques_id_id", id)
        .withAll()
        .get();
  },

  educationalgroupById(_, { query }) {
    return id =>
      query()
        .withAll()
        .find(id);
  },

  filieres(_, { query }) {
    let allGroup = query()
      .withAll()
      .get();
    let filieres = allGroup.map(group => group.filiere);
    return _countBy(filieres);
  },

  filieresByYear(_, { query }) {
    return id => {
      let allGroup = query()
        .where("annee_academiques_id_id", id)
        .withAll()
        .get();
      let filieres = allGroup.map(group => group.filiere);
      return _countBy(filieres);
    };
  }
};

export default getters;
