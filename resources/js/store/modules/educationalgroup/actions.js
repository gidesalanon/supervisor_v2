import api from '@/api'

const actions = {

  fetch: ({ dispatch }) => {
    return api.group().then((response) => {
      if (response.isSuccessful) {
        const data = response.data
        dispatch('insertOrUpdate', { data })
      }
      return response
    }).catch((error) => {
      console.error(error)
      throw error
    })
  },

  store: ({ dispatch }, {
    annee_academiques_id,
    owner,
    classroom_chief,
    grade,
    filiere
  }) => {
    return api.group.create({
      annee_academiques_id,
      owner,
      classroom_chief,
      grade,
      filiere
    }).then((response) => {
      if (response.isSuccessful) {
        const data = response.data
        dispatch('insertOrUpdate', { data })
      }
      return response
    }).catch((error) => {
      console.error(error)
      throw error
    })
  },

  remove: ({ dispatch }, id) => {
    return api.group.delete(id).then((response) => {
      if (response.data) {
        dispatch('delete', id)
      }
      return response
    }).catch((error) =>  {
      console.error(error)
      throw error
    })
  }
}

export default actions
