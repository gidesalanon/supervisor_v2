import api from '@/api'

export default {

  fetch: ({ dispatch }) => {
    return api.academicyear().then((response) => {
      if (response.isSuccessful) {
        const { data } = response
        dispatch('insertOrUpdate', { data })
      }
      return response
    }).catch(error => {
      console.error(error)
      throw error
    })
  },

  create: ({ dispatch }, { name }) => {
    return api.academicyear.create({ name }).then((response) => {
      if (response.isSuccessful) {
        const { data } = response
        dispatch('insertOrUpdate', { data })
      }
      return response
    }).catch((error) => {
      console.error(error)
      throw error
    })
  },

  update: ({ dispatch }, {id, name}) => {
    return api.academicyear.update(id, { name }).then((response) => {
      if (response.isSuccessful) {
        const { data } = response
        dispatch('insertOrUpdate', { data })
      }
      return response
    }).catch((error) => {
      console.error(error)
      throw error
    })
  },

  remove: ({ dispatch }, id ) => {
    return api.academicyear.delete(id).then((response) => {
      if (response.isSuccessful) {
        dispatch('delete', id)
      }
      return response
    }).catch((error) => {
      console.error(error)
      throw error
    })
  }

}
