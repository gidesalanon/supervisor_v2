export default {
  years (_, { query }) {
    return query().get()
  }
}
