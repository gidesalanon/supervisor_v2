export default {
  ues: (_, { query }) => {
    return query()
      .withAll()
      .get();
  },

  uesByEducationalGroup(_, { query }) {
    return id =>
      query()
        .where("group_id", parseInt(id))
        .withAll()
        .get();
  },

  useById(_, { query }) {
    return id =>
      query()
        .where("id", id)
        .withAll()
        .get();
  }
};
