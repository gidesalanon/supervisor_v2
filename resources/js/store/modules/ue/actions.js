import api from "@/api";

const actions = {
  create: ({ dispatch }, { name, code, group_id }) => {
    return api.ue
      .create({ name, code, group_id })
      .then(response => {
        if (response.data) {
          const ue = response.data;
          dispatch("insertOrUpdate", { data: ue });
        }
        return response;
      })
      .catch(error => {
        console.error(error);
        throw error;
      });
  },

  fetch: ({ dispatch }) => {
    return api.ue
      .all()
      .then(response => {
        if (response.data) {
          const data = response.data;
          dispatch("insertOrUpdate", { data });
        }
        return response;
      })
      .catch(error => {
        console.error(error);
        throw error;
      });
  },

  remove: ({ dispatch }, id) => {
    return api.ue
      .delete(id)
      .then(response => {
        if (response.data) {
          dispatch("delete", id);
        }
        return response;
      })
      .catch(error => {
        console.error(error);
        throw error;
      });
  }
};

export default actions;
