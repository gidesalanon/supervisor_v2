import api from '@/api'
import User from '@/store/models/User'

import { AUTH_ACCESS_TOKEN_KEY } from '@/constants'

const state = {
  userId: null,
  accessToken: null,
  termsAccepted: null,
  defaultPasswordChanged: null
}

const getters = {
  userId (state) {
    return state.userId
  },

  accessToken (state) {
    return state.accessToken
  },

  authenticated (state) {
    return !!state.accessToken
  },

  user (state) {
    return User.find(state.userId)
  }
}

const actions = {
  init ({ commit }) {
    const accessToken = localStorage.getItem(AUTH_ACCESS_TOKEN_KEY)

    if (accessToken) {
      commit('ACCESS_TOKEN', accessToken)
      api.setAccessToken(accessToken)
    }
  },

  check ({ commit }) {
    return api.me().then(response => {
      if (response.isSuccessful) {
        const user = response.data
        commit('USER_ID', user.id)
        User.insertOrUpdate({ data: user })
      }
      return response
    })
  },

  login ({ commit }, { email, password }) {
    return api.login.create({ email, password })
      .then(response => {
        if (response.isSuccessful) {
          const { user, access_token } = response.data
          commit('ACCESS_TOKEN', access_token)
          commit('USER_ID', user.id)
          User.insertOrUpdate({ data: user })
        }

        return response
      })
  },

  logout ({ commit }) {
    commit('ACCESS_TOKEN', null)
    localStorage.clear()
  }
}

const mutations = {
  USER_ID (state, userId) {
    state.userId = userId
  },

  ACCESS_TOKEN (state, accessToken) {
    state.accessToken = accessToken
    api.setAccessToken(accessToken)
    localStorage.setItem(AUTH_ACCESS_TOKEN_KEY, accessToken)
  }
}

export default {
  state,
  getters,
  actions,
  mutations,
  name: 'auth',
  namespaced: true
}
