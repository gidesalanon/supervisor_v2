export default {
  users (_, { query }) {
    return query().get()
  },

  owners (_, { query }) {
    return query().where('role', 'resp.gped').get()
  },

  classroomChiefs (_, { query }) {
    return query().where('role', 'resp.class').get()
  },

  admins (_, { query }) {
    return query().where('role', 'admin').get()
  }

}
