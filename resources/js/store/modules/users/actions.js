import api from "@/api";

export default {
  getUsers({ dispatch }) {
    return api["user/read/all"]()
      .then(response => {
        if (response.isSuccessful) {
          const data = response.data;
          dispatch("insertOrUpdate", { data });
        }
        return response;
      })
      .catch(error => {
        console.error(error);
        throw error;
      });
  },

  create({ dispatch }, data) {
    return api["user/create"]
      .create(data)
      .then(response => {
        if (response.isSuccessful) {
          const data = response.data;
          dispatch("insertOrUpdate", { data });
        }
        return response;
      })
      .catch(error => {
        console.error(error);
        throw error;
      });
  },

  update({ dispatch }, data) {
    return api["user/modify"]
      .update(data.id, data)
      .then(response => {
        if (response.isSuccessful) {
          const data = response.data;
          dispatch("insertOrUpdate", { data });
        }
        return response;
      })
      .catch(error => {
        console.error(error);
        throw error;
      });
  }
};
