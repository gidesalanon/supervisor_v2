import api from "@/api";

const actions = {
  create({ dispatch }, { ue_id, hourlyMass, name, professor }) {
    return api.ec
      .create({
        ue_id,
        hourlyMass,
        name,
        professor
      })
      .then(response => {
        if (response.isSuccessful) {
          const data = response.data;
          dispatch("insertOrUpdate", { data });
        }
        return response;
      })
      .catch(error => {
        console.error(error);
        throw error;
      });
  },

  update(
    { dispatch },
    { id, ue_id, status, hourlyMass, hourlyMassDone, name, professor }
  ) {
    return api.ec
      .update(id, {
        ue_id,
        status,
        hourlyMass,
        hourlyMassDone,
        name,
        professor
      })
      .then(response => {
        if (response.isSuccessful) {
          const data = response.data;
          dispatch("insertOrUpdate", { data });
        }
        return response;
      })
      .catch(error => {
        console.error(error);
        throw error;
      });
  },

  fetchById({ dispatch }, { id }) {
    api.ec
      .find(id)
      .then(response => {
        if (response.isSuccessful) {
          const data = response.data[0];
          dispatch("insertOrUpdate", { data });
        }
        return response;
      })
      .catch(error => {
        console.error(error);
        throw error;
      });
  },

  fetch({ dispatch }) {
    api
      .ec()
      .then(response => {
        if (response.isSuccessful) {
          const data = response.data[0];
          dispatch("insertOrUpdate", { data });
        }
        return response;
      })
      .catch(error => {
        console.error(error);
        throw error;
      });
  },

  remove: ({ dispatch }, id) => {
    return api.ec
      .delete(id)
      .then(response => {
        if (response.data) {
          dispatch("delete", id);
        }
        return response;
      })
      .catch(error => {
        console.error(error);
        throw error;
      });
  }
};

export default actions;
