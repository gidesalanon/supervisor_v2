export default {
  ecById(_, { query }) {
    return id =>
      query()
        .whereId(id)
        .first();
  },

  ecsByUeId(_, { query }) {
    return id =>
      query()
        .where("ue_id", id)
        .withAll()
        .get();
  }
};
