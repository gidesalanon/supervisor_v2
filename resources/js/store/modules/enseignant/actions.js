import api from '@/api'

export default {

  fetch: ({ dispatch }) => {
    return api.enseignant().then((response) => {
      if (response.isSuccessful) {
        const { data } = response
        dispatch('insertOrUpdate', { data })
      }
      return response
    }).catch(error => {
      console.error(error)
      throw error
    })
  },

  create: ({ dispatch }, item) => {
    return api.enseignant.create(item).then((response) => {
      if (response.isSuccessful) {
        const { data } = response
        dispatch('insertOrUpdate', { data })
      }
      return response
    }).catch((error) => {
      console.error(error)
      throw error
    })
  },

  update: ({ dispatch }, {id, name}) => {
    return api.enseignant.update(id, { name }).then((response) => {
      if (response.isSuccessful) {
        const { data } = response
        dispatch('insertOrUpdate', { data })
      }
      return response
    }).catch((error) => {
      console.error(error)
      throw error
    })
  },

  remove: ({ dispatch }, id ) => {
    return api.enseignant.delete(id).then((response) => {
      if (response.isSuccessful) {
        dispatch('delete', id)
      }
      return response
    }).catch((error) => {
      console.error(error)
      throw error
    })
  }

}
