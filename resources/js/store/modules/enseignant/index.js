
import actions from './actions'
import getters from './getters'
export default {
  getters,
  actions,
  namespaced: true
}
