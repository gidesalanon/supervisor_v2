import VuexORM from '@vuex-orm/core'
import { getField, updateField } from 'vuex-map-fields'

import api from '../api'

const requireModel = require.context('./models', false, /^\.\/.*\.js$/)
const database = new VuexORM.Database()

const getModelModule = (model) => {
  let module

  try {
    module = require(`./modules/${model.entity}`).default
  } catch (e) {
    module = {}
  }

  module.state = {
    ...(module.state || {})
  }

  module.getters = {
    ...(module.getters || {}),
    getField
  }

  module.mutations = {
    ...(module.mutations || {}),
    updateField
  }

  module.actions = {
    ...(module.actions || {})
  }

  return module
}

requireModel.keys()
  .forEach(filename => {
    const modelClass = requireModel(filename).default

    database.register(
      modelClass,
      getModelModule(modelClass)
    )

    if (modelClass.endpoint) {
      api.endpoint(modelClass.endpoint, modelClass.entity)
    }
  })

export default database
