import Vue from 'vue'
import Vuex from 'vuex'
import VuexORM from '@vuex-orm/core'
import database from './database'

import AuthModule from './modules/auth'
// import Global from './modules/global'
import notification from './modules/notification'

Vue.use(Vuex)

const store = new Vuex.Store({
  ...notification,
  modules: {
    [AuthModule.name]: AuthModule,
    // [Global.name]: Global
  },

  plugins: [
    VuexORM.install(database)
  ]
})

export default store
