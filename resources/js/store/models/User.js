import { Model } from "@vuex-orm/core";

export default class User extends Model {
  static entity = "users";

  static Types = {
    ADMIN: "admin"
  };

  static state() {
    return {
      // searchable: ['firstname', 'lastname', 'email']
    };
  }

  static fields() {
    return {
      id: this.string(),
      role: this.string(),
      name: this.string(),
      email: this.string(),
      phone: this.string(),
      defaultPasswordChanged: this.boolean(false),
      createdAt: this.number(),
      updatedAt: this.number()
    };
  }

  get isAdmin() {
    return this.role === "admin";
  }

  get isOwner() {
    return this.role === "resp.gped";
  }

  get isChief() {
    return this.role === "resp.class";
  }

  get isGuest() {
    return this.role === "viewer";
  }
  get isClassic() {
    return this.isAdmin || this.isOwner || this.isChief;
  }
}
