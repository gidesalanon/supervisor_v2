import { Model } from '@vuex-orm/core'
import Ec from './Ec'
import EducationalGroup from './EducationalGroup'

export default class Ue extends Model {

  static entity = 'ue'

  static fields () {
    return  {
      id: this.string(''),
      name: this.string(''),
      code: this.string(''),
      ecs: this.hasMany(Ec, 'ue_id'),
      group_id: this.attr(null),
      educationalgroup: this.belongsTo(EducationalGroup, 'group_id')
    }
  }
}
