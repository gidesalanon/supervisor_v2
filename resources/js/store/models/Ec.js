import { Model } from '@vuex-orm/core'
import Ue from './Ue'

export default class Ec extends Model {
  static entity = 'ec'

  static fields () {
    return {
      id: this.attr(''),
      name: this.string(),
      status: this.string(),
      hourlyMass: this.string(),
      hourlyMassDone: this.string(),
      professor: this.string(),
      ue_id: this.string(),
      ue: this.belongsTo(Ue, 'ue_id')
    }
  }

  get profil () {
    return this.hourlyMass === this.hourlyMassDone ? 'complet' : this.status
  }
}
