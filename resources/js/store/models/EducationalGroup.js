import { Model } from '@vuex-orm/core'
import User from './User'
import AcademicYear from './AcademicYear'

export default class EducationnalGroup extends Model {

  static entity = 'educationalgroup'

  static fields () {
    return {
      id: this.attr(null),
      grade: this.string(),
      filiere: this.string(),
      owner_id: this.attr(null),
      owner: this.belongsTo(User, 'owner_id'),
      classroom_chief_id: this.attr(null),
      classroom_chief: this.belongsTo(User, 'classroom_chief_id'),
      annee_academiques_id_id: this.attr(null),
      annee_academiques_id: this.belongsTo(AcademicYear, 'annee_academiques_id_id')
    }
  }
}
