import { Model } from '@vuex-orm/core'
import moment from 'moment'
import User from './User'

export default class History extends Model {
  static entity = 'history'

  static fields () {
    return {
      id: this.attr(null),
      log_name: this.string(),
      description: this.attr(''),
      causer_id: this.attr(null),
      user: this.belongsTo(User, 'causer_id'),
      causer_type: this.string(),
      created_at: this.attr(''),
      updated_at: this.attr('')
    }
  }

  date () {
    return moment(this.created_at).format('LLL')
  }
}
