import { Model } from '@vuex-orm/core'

export default class AcademicYear extends Model {
  static entity = 'academicyear'

  static fields () {
    return {
      id: this.attr(null),
      name: this.attr('')
    }
  }
}
