import { Model } from "@vuex-orm/core";

export default class Enseignant extends Model {
  static entity = "enseignant";
  static fields() {
    return {
      id: this.string(),
      name: this.string(),
      email: this.string(),
      phone: this.string(),
    };
  }
}
