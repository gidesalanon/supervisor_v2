<?php

use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

      DB::table('users')->insert([
            'name' => 'Administrateur',
            'email' => 'admin@ifri.net',
            'password' => bcrypt('Admin2019!'),
            'role' => 'admin',
            'phone' => '96295073',
        ]);
    }
}
