<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateGroupePedagogiquesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('groupe_pedagogiques', function (Blueprint $table) {
            $table->bigIncrements('id');
            // $table->string('name');
            $table->enum('grade', explode(',', env("GRADE")));
            $table->enum('filiere', explode(',', env("FILIERE")));
            $table->bigInteger('owner')->unsigned()->nullable();
            $table->foreign('owner')->references('id')->on('users')->onDelete('cascade');
            $table->bigInteger('classroom_chief')->unsigned()->nullable();
            $table->foreign('classroom_chief')->references('id')->on('users')->onDelete('cascade');
            $table->enum('state', ['created', 'deleted'])->default('created');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('groupe_pedagogiques');
    }
}
