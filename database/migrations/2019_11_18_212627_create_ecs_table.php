<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateEcsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('ecs', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('name');
            $table->string('hourlyMass');
            $table->string('hourlyMassDone')->nullable();
            $table->enum('status', ['pending', 'start', 'complete'])->default('pending');
            $table->enum('state', ['created', 'deleted'])->default('created');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('ecs');
    }
}
