<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class ModifyUesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
      Schema::table('ues', function (Blueprint $table) {
        $table->bigInteger('group_id')->unsigned()->nullable()->after('id');
        $table->foreign('group_id')->references('id')->on('groupe_pedagogiques')->onDelete('cascade');
      });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
      Schema::table('ues', function (Blueprint $table) {
        $table->dropForeign(['group_id']);
      });
    }
}
