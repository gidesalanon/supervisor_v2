<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class ModifyGroupePedagogiquesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('groupe_pedagogiques', function (Blueprint $table) {
            $table->bigInteger('annee_academiques_id')->unsigned()->nullable()->after('id');
            $table->foreign('annee_academiques_id')->references('id')->on('annee_academiques')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('groupe_pedagogiques', function (Blueprint $table) {
            $table->dropForeign(['annee_academiques_id']);
        });
    }
}
