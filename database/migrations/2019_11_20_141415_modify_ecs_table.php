<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class ModifyEcsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('ecs', function (Blueprint $table) {
            $table->bigInteger('ue_id')->unsigned()->nullable()->after('id');
            $table->foreign('ue_id')->references('id')->on('ues')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('ecs', function (Blueprint $table) {
            $table->dropForeign(['ue_id']);
        });
    }
}
