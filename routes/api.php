<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::post('/user/create', 'Auth\RegisterController@create');
Route::post('/login', 'Auth\indexController@login');
Route::post('/logout', 'Auth\indexController@logout');
Route::get('/me', 'Auth\indexController@me');
Route::get('/refresh', 'Auth\indexController@refresh');
Route::put('/user/modify/{id}', 'Auth\RegisterController@modify');
Route::delete('/user/delete/{id}', 'Auth\RegisterController@delete');
Route::get('/user/read/all', 'Auth\RegisterController@readall');
Route::get('/user/read/{id}', 'Auth\RegisterController@read');
Route::resource('history', 'Auth\HistoryController');
Route::resource('academicyear', 'AnneeAcademique\AnneeAcademiqueController');
Route::resource('ue', 'Ue\UeController');
Route::resource('ec', 'Ec\EcController');
Route::resource('group', 'GroupePedagogique\GroupePedagogiqueController');
Route::resource('enseignant', 'EnseignantController');
