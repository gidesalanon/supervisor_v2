<?php

namespace App\Http\Controllers\Ue;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Ue;
use App\Models\Ec;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Redirect;

class UeController extends Controller
{
    public function __construct()
    {
      $this->middleware('jwt.auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $ue = Ue::where(['state' => 'created'])->with('ecs')->with('groupePedagogique')->get();

        return $ue;
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $messages = [
            'required' => 'Le champs :attribute est requis.',
            'group_id.exists' => 'L\'identifiant n\'existe pas.',
            'string' => 'Le champs :attribute doit être du texte',
        ];
        $validator = Validator::make(
            $request->all(),
            [
              'name' => 'required|string',
              'code' => 'required|string',
              'group_id' => 'required|exists:groupe_pedagogiques,id'
            ],
            $messages
        );

        if ($validator->fails()) {
            // return Redirect::back()->withErrors($validator)->withInput();
            return $validator->errors();
        }

        // dd($request->all());
        $ue = Ue::create([
            'group_id' => $request->group_id,
            'name' => $request->name,
            'code' => $request->code
        ]);

        return $ue;
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $ue = Ue::where(['state' => 'created', 'id' => $id])->with('ecs')->with('groupePedagogique')->get();
        return $ue;
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $ue = Ue::findOrFail($id);

        $ue->update([
          'name' => $request->name,
        ]);

        return response()->json($ue);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $ue = Ue::findOrFail($id);
        $ue->delete();//update(['state' => 'deleted']);
        $ecs = Ec::where(['ue_id' => $ue->id])->get();
        foreach($ecs as $ec) {
            $ec->delete();//update(['state' => 'deleted']);
        }
        return "Suppression effectuée avec succès";
    }
}
