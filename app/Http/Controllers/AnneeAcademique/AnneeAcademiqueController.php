<?php

namespace App\Http\Controllers\AnneeAcademique;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\AnneeAcademique;
use App\Models\GroupePedagogique;
use App\Models\Ue;
use App\Models\Ec;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Redirect;

class AnneeAcademiqueController extends Controller
{
    public function __construct()
    {
      $this->middleware('jwt.auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $annee = AnneeAcademique::where(['state' => 'created'])->get();

        return $annee;
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $messages = [
            'required' => 'Le champs :attribute est requis.',
            'string' => 'Le champs :attribute doit être du texte',
        ];
        $validator = Validator::make(
            $request->all(),
            [
                'name' => 'required|string',
            ],
            $messages
        );

        if ($validator->fails()) {
            // return Redirect::back()->withErrors($validator)->withInput();
            return response()->json([
              $validator->errors()
            ], 400);
        }

        // dd($request->all());
        $annee = AnneeAcademique::create([
            'name' => $request->name,
        ]);

        return $annee;
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $annee = AnneeAcademique::where(['state' => 'created', 'id' => $id])->get();

        return $annee;
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $annee = AnneeAcademique::findOrFail($id);

        $annee->update([
            'name' => $request->name,
        ]);

        return "Modification effectuée avec succès";
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $annee = AnneeAcademique::findOrFail($id);
        $annee->delete();//update(['state' => 'deleted']);
        $groups = GroupePedagogique::where(['annee_academiques_id' => $id])->get();        
        foreach($groups as $group) {
            $group->delete();//update(['state' => 'deleted']);
            $ues = Ue::where(['group_id' => $group->id])->get();
            foreach($ues as $ue) {
                $ue->delete();//update(['state' => 'deleted']);
                $ecs = Ec::where(['ue_id' => $ue->id])->get();
                foreach($ecs as $ec) {
                    $ec->delete();//update(['state' => 'deleted']);
                }
            }
        }
        return "Suppression effectuée avec succès";
    }
}
