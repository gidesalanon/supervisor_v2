<?php

namespace App\Http\Controllers\Ec;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Ec;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Redirect;

class EcController extends Controller
{
    public function __construct()
    {
      $this->middleware('jwt.auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $ec = Ec::where(['state' => 'created'])->get();

        return $ec;
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $messages = [
            'required' => 'Le champs :attribute est requis.',
            'ue_id.exists' => 'L\'identifiant n\'existe pas.',
            'hourlyMass.numeric' => 'La masse horaire est numérique.',
            'hourlyMass.min' => 'La masse horaire doit contenir au moins 1 chiffre.',
            'string' => 'Le champs :attribute doit être du texte',
        ];
        $validator = Validator::make(
            $request->all(),
            [
                'name' => 'required|string',
                'professor' => 'required|string',
                'ue_id' => 'required|exists:ues,id',
                'hourlyMass' => 'required|numeric|min:1',
            ],
            $messages
        );

        if ($validator->fails()) {
            // return Redirect::back()->withErrors($validator)->withInput();
            return $validator->errors();
        }

        // dd($request->all());
        $ec = Ec::create([
            'ue_id' => $request->ue_id,
            'name' => $request->name,
            'professor' => $request->professor,
            'status' => 'pending',
            'hourlyMass' => $request->hourlyMass,
            'hourlyMassDone' => '0'
        ]);

        return $ec;
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $ec = Ec::where(['state' => 'created', 'id' => $id])->get();

        return $ec;
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $ec = Ec::findOrFail($id);

        $ec->update([
            'ue_id' => $request->ue_id,
            'name' => $request->name,
            'professor' => $request->professor,
            'hourlyMass' => $request->hourlyMass,
            'hourlyMassDone' => $request->hourlyMassDone,
            'status' => $request->status,
        ]);

        return $ec;
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $ec = Ec::findOrFail($id);

        $ec->delete();//update(['state' => 'deleted']);

        return "Suppression effectuée avec succès";
    }
}
