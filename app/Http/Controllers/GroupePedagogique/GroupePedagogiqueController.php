<?php

namespace App\Http\Controllers\GroupePedagogique;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\GroupePedagogique;
use App\Models\Ue;
use App\Models\Ec;
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\Rule;
use Illuminate\Support\Facades\Redirect;

class GroupePedagogiqueController extends Controller
{
    public function __construct()
    {
        $this->middleware('jwt.auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $group = GroupePedagogique::where(['state' => 'created'])
            ->with('owner')
            ->with('classroomChief')
            ->with('anneeAcademique')
            ->get();

        return $group;
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $messages = [
            'required' => 'Le champs :attribute est requis.',
            'annee_academiques_id.exists' => 'L\'identifiant n\'existe pas.',
            'owner.exists' => 'L\'identifiant n\'existe pas.',
            'string' => 'Le champs :attribute doit être du texte',
            'classroom_chief.exists' => 'L\'identifiant n\'existe pas.',
        ];
        $validator = Validator::make(
            $request->all(),
            [
                'annee_academiques_id' => 'required|exists:annee_academiques,id',
                'grade' => ['required', 'string', Rule::in(explode(',', env("GRADE")))],
                'filiere' => ['required', 'string', Rule::in(explode(',', env("FILIERE")))],
                'owner' => 'required|exists:users,id',
                'classroom_chief' => 'required|exists:users,id',
            ],
            $messages
        );

        if ($validator->fails()) {
            // return Redirect::back()->withErrors($validator)->withInput();
            return $validator->errors();
        }

        $existGroup = GroupePedagogique::where([
          'state' => 'created',
          'annee_academiques_id' => $request->annee_academiques_id,
          'grade' => $request->grade,
          'filiere' => $request->filiere,
          ])->get();

          // if (empty($existGroup)) {
            $group = GroupePedagogique::create([
              'annee_academiques_id' => $request->annee_academiques_id,
              // 'name' => $request->name,
              'grade' => $request->grade,
              'filiere' => $request->filiere,
              'owner' => $request->owner,
              'classroom_chief' => $request->classroom_chief,
            ]);

            return $group;
          // } else {
          //   return response()->json([
          //     'message' => 'Ce groupe pédagogique existe déjà'
          //   ])->setStatusCode(400);
          // }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $group = GroupePedagogique::where(['state' => 'created', 'id' => $id])
            ->with('owner')
            ->with('classroomChief')
            ->with('anneeAcademique')
            ->get();
        return $group;
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $group = GroupePedagogique::findOrFail($id);

        $group->update([
            'annee_academiques_id' => $request->annee_academiques_id,
            // 'name' => $request->name,
            'grade' => $request->grade,
            'filiere' => $request->filiere,
            'owner' => $request->owner,
            'classroom_chief' => $request->classroom_chief,
        ]);

        return "Modification effectuée avec succès";
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $group = GroupePedagogique::findOrFail($id);
        $group->delete();//update(['state' => 'deleted']);
        $ues = Ue::where(['group_id' => $group->id])->get();
        foreach ($ues as $ue) {
            $ue->delete();//update(['state' => 'deleted']);
            $ecs = Ec::where(['ue_id' => $ue->id])->get();
            foreach($ecs as $ec) {
                $ec->delete();//update(['state' => 'deleted']);
            }
        }
        return "Suppression effectuée avec succès";
    }
}
