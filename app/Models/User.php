<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Tymon\JWTAuth\Contracts\JWTSubject;
use Illuminate\Support\Str;
use Spatie\Activitylog\Traits\LogsActivity;
use Illuminate\Database\Eloquent\SoftDeletes;

class User extends Authenticatable implements JWTSubject
{
    use Notifiable;
    use LogsActivity;
    use SoftDeletes;

    /**
     * The database table used by the model.
     *
     * @var string
     */
    public $table = "users";

    /**
     * The primary key for the model.
     *
     * @var string
     */
    protected $primaryKey = 'id';

    public $timestamps = true;

    protected $fillable = [
        'name',
        'email',
        'phone',
        'password',
        'role',
        'state',
        'created_at',
        'updated_at'
    ];

    protected static $logAttributes = [
        'name',
        'email',
        'phone',
        'password',
        'role',
        'state',
        'created_at',
        'updated_at'
    ];

    // Customizing the log name 
    protected static $logName = 'Action sur utilisateur';

    protected $dates = ['deleted_at'];

    // Customizing the description
    // public function getDescriptionForEvent(string $eventName): string
    // {
    //     return "La table a été {$eventName}";
    // }

    protected static $logFillable = true;

    public function getJWTIdentifier()
    {
        return $this->getKey();
    }

    public function getJWTCustomClaims()
    {
        return [];
    }
    public function setPasswordAttribute($password)
    {
        if (!empty($password)) {
            $this->attributes['password'] = bcrypt($password);
        }
    }

    /**
     *  Setup model event hooks
     */
    public static function boot()
    {
        parent::boot();
        self::creating(function ($model) {
            // $model->id = Str::uuid()->toString();
        });
    }
}
