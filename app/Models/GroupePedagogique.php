<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Spatie\Activitylog\Traits\LogsActivity;
use Illuminate\Database\Eloquent\SoftDeletes;

class GroupePedagogique extends Model
{
    use LogsActivity;
    use SoftDeletes;

    /**
     * The database table used by the model.
     *
     * @var string
     */
    public $table = "groupe_pedagogiques";

    /**
     * The primary key for the model.
     *
     * @var string
     */
    protected $primaryKey = 'id';

    public $timestamps = true;

    protected static $logFillable = true;

    protected $fillable = [
        'grade',
        'filiere',
        'annee_academiques_id',
        'owner',
        'classroom_chief',
        'state',
        'created_at',
        'updated_at'
    ];

    protected static $logAttributes = [
        'grade',
        'filiere',
        'annee_academiques_id',
        'owner',
        'classroom_chief',
        'state',
        'created_at',
        'updated_at'
    ];

    // Customizing the log name 
    protected static $logName = 'Action sur groupe pédagogique';

    protected $dates = ['deleted_at'];

    // Customizing the description
    // public function getDescriptionForEvent(string $eventName): string
    // {
    //     return "La table a été {$eventName}";
    // }


    public function owner()
    {
        return $this->belongsTo('App\Models\User', 'owner', 'id');
    }

    public function classroomChief()
    {
        return $this->belongsTo('App\Models\User', 'classroom_chief', 'id');
    }

    public function anneeAcademique()
    {
        return $this->belongsTo('App\Models\AnneeAcademique', 'annee_academiques_id', 'id');
    }

    public function ue()
    {
        return $this->hasMany('App\Models\Ue');
    }
}
