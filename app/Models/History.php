<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class History extends Model
{
     /**
     * The database table used by the model.
     *
     * @var string
     */
    public $table = "history";

	    /**
     * The primary key for the model.
     *
     * @var string
     */
    protected $primaryKey = 'id';

    public $timestamps = true;

    protected $fillable = [
        'user_id',
        'action',
        'date',
        'state',
        'created_at',
        'updated_at'
    ];

    public function user() {
        return $this->belongsTo('App\Models\User', 'user_id', 'id');
    }
}
