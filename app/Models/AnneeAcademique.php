<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Spatie\Activitylog\Traits\LogsActivity;
use Illuminate\Database\Eloquent\SoftDeletes;

class AnneeAcademique extends Model
{
    use LogsActivity;
    use SoftDeletes;

    /**
     * The database table used by the model.
     *
     * @var string
     */
    public $table = "annee_academiques";

    /**
     * The primary key for the model.
     *
     * @var string
     */
    protected $primaryKey = 'id';

    public $timestamps = true;

    protected static $logFillable = true;

    protected $fillable = [
        'name',
        'state',
        'created_at',
        'updated_at'
    ];

    protected static $logAttributes = [
        'name',
        'state',
        'created_at',
        'updated_at'
    ];

    // Customizing the log name 
    protected static $logName = 'Action sur année académique';

    protected $dates = ['deleted_at'];

    // Customizing the description
    // public function getDescriptionForEvent(string $eventName): string
    // {
    //     return "La table a été {$eventName}";
    // }

    public function groupePedagogique()
    {
        return $this->hasMany('App\Models\GroupePedagogique');
    }
}
