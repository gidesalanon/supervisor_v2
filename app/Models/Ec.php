<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Spatie\Activitylog\Traits\LogsActivity;
use Illuminate\Database\Eloquent\SoftDeletes;

class Ec extends Model
{
    use LogsActivity;
    use SoftDeletes;

    /**
     * The database table used by the model.
     *
     * @var string
     */
    public $table = "ecs";

    /**
     * The primary key for the model.
     *
     * @var string
     */
    protected $primaryKey = 'id';

    public $timestamps = true;

    protected static $logFillable = true;

    protected $fillable = [
        'name',
        'professor',
        'ue_id',
        'hourlyMass',
        'hourlyMassDone',
        'status',
        'state',
        'created_at',
        'updated_at'
    ];

    protected static $logAttributes = [
        'name',
        'professor',
        'ue_id',
        'hourlyMass',
        'hourlyMassDone',
        'status',
        'state',
        'created_at',
        'updated_at'
    ];

    // Customizing the log name 
    protected static $logName = 'Action sur EC';

    protected $dates = ['deleted_at'];

    // Customizing the description
    // public function getDescriptionForEvent(string $eventName): string
    // {
    //     return "La table a été {$eventName}";
    // }


    public function ue()
    {
        return $this->belongsTo('App\Models\Ue', 'ue_id', 'id');
    }
}
