<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Enseignant extends Model
{
    /**
     * The database table used by the model.
     *
     * @var string
     */
    public $table = "enseignants";

    /**
     * The primary key for the model.
     *
     * @var string
     */
    protected $primaryKey = 'id';

    public $timestamps = true;

    protected $fillable = [
        'name',
        'email',
        'phone',
        'created_at',
        'updated_at'
    ];

    protected static $logAttributes = [
        'name',
        'email',
        'phone',
        'created_at',
        'updated_at'
    ];

    // Customizing the log name 
    protected static $logName = 'Action sur enseignant';

    protected $dates = ['deleted_at'];

}
